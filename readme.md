# Spring with SLF4J and Log4J

## Description of the problem

[Apache Log4J][log4j] is used in a Spring project.

There is this error:
```log
SLF4J: No SLF4J providers were found.
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See https://www.slf4j.org/codes.html#noProviders for further details.
SLF4J: Class path contains SLF4J bindings targeting slf4j-api versions 1.7.x or earlier.
SLF4J: Ignoring binding found at [jar:file:/Users/maherkarim/.gradle/caches/modules-2/files-2.1/org.apache.logging.log4j/log4j-slf4j-impl/2.20.0/7ab4f082fd162f60afcaf2b8744a3d959feab3e8/log4j-slf4j-impl-2.20.0.jar!/org/slf4j/impl/StaticLoggerBinder.class]
SLF4J: See https://www.slf4j.org/codes.html#ignoredBindings for an explanation.
```

[log4j]: https://logging.apache.org/log4j/2.x/ "Apache Log4j™ 2"

## How to build and test

```bash
./gradlew clean test
```

## How to run

```bash
./gradlew bootRun
```

then run in another terminal

```bash
curl http://localhost:8080/hello/John
```

## How to fix the logging configuration issue

### Solution 1

Replace `log4j-slf4j-impl` with `log4j-slf4j2-impl`. This library supports SLF4J 2.* that is used is Spring.

### Solution 2

Remove lines

```groovy
configurations.configureEach {
    exclude group: 'org.springframework.boot', module: 'spring-boot-starter-logging'
}
```