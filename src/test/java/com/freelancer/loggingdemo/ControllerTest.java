package com.freelancer.loggingdemo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(value = HelloWorld.class)
@ContextConfiguration(classes = LoggingDemoApplication.class)
class ControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void givenLoginForm_whenEmailFieldNotProvided_testCustomValidMessageIsReturned()
      throws Exception {
    var result = mockMvc.perform(get("/hello/John")).andExpect(status().isOk()).andReturn();

    assertEquals("Hello, John", result.getResponse().getContentAsString());
  }

}