package com.freelancer.loggingdemo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class HelloWorld {

  @GetMapping("/hello/{name}")
  public String hello(@PathVariable(value = "name") String name) {
    log.info("Get name {}", name);
    return "Hello, " + name;
  }

}
